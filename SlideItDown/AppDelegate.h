//
//  AppDelegate.h
//  SlideItDown
//
//  Created by Mike Finney on 1/15/15.
//  Copyright (c) 2015 CARFAX. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat heightOfLabel = 44;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

