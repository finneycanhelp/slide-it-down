//
//  SIDSecondViewController.m
//  SlideItDown
//
//  Created by Mike Finney on 1/15/15.
//  Copyright (c) 2015 CARFAX. All rights reserved.
//

#import "SIDSecondViewController.h"
#import "AppDelegate.h"

@implementation SIDSecondViewController

- (IBAction)slideItDownTapped:(id)sender {

    CGRect windowFrame = UIApplication.sharedApplication.delegate.window.frame;
    
    windowFrame.origin.y = heightOfLabel;
    
    UIApplication.sharedApplication.delegate.window.frame = windowFrame;
}


@end
